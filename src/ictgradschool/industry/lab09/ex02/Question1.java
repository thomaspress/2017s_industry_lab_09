package ictgradschool.industry.lab09.ex02;

/**
 * Created by tpre939 on 4/12/2017.
 */
public class Question1 {

    public static Circle circleTest = new Circle(12);

    public static void main(String[] args) {
        Question1 program = new Question1();
        System.out.println(program.start(circleTest));
    }

    private <T> String start(T input){
        return input.toString();
    }

}
